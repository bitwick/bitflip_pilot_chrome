// Copyright (c) 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//flipkart=0, snapdeal=1, amazon=2
/**
 * Get the current URL.
 *
 * @param {function(string)} callback - called when the URL of the current tab
 *   is found.
 */
function getCurrentTabUrl(callback) {
  // Query filter to be passed to chrome.tabs.query - see
  // https://developer.chrome.com/extensions/tabs#method-query
  var queryInfo = {
    active: true,
    currentWindow: true
  };

  chrome.tabs.query(queryInfo, function(tabs) {
    // chrome.tabs.query invokes the callback with a list of tabs that match the
    // query. When the popup is opened, there is certainly a window and at least
    // one tab, so we can safely assume that |tabs| is a non-empty array.
    // A window can only have one active tab at a time, so the array consists of
    // exactly one tab.
    var tab = tabs[0];

    // A tab is a plain object that provides information about the tab.
    // See https://developer.chrome.com/extensions/tabs#type-Tab
    var url = tab.url;
    website = -1;
    url = url.replace("http://www.", "");
    url = url.replace("https://www.", "");
    url = url.replace("https://", "");
    url = url.replace("http://", "");
    if(!url.startsWith("flipkart.com") && !url.startsWith("snapdeal.com")) {
      url = "";
      renderStatus('Cannot display...not flipkart or snapdeal');
      return;
    }
    else if(url.startsWith("flipkart.com")) {
      website = 0;
    }
    else if(url.startsWith("snapdeal.com")) {
      website = 1;
    }

    // tab.url is only available if the "activeTab" permission is declared.
    // If you want to see the URL of other tabs (e.g. after removing active:true
    // from |queryInfo|), then the "tabs" permission is required to see their
    // "url" properties.
    console.assert(typeof url == 'string', 'tab.url should be a string');

    callback(url,website);
  });

  // Most methods of the Chrome extension APIs are asynchronous. This means that
  // you CANNOT do something like this:
  //
  // var url;
  // chrome.tabs.query(queryInfo, function(tabs) {
  //   url = tabs[0].url;
  // });
  // alert(url); // Shows "undefined", because chrome.tabs.query is async.
}

function getImageUrl(pid, price, website, callback, errorCallback) {
  var feast = "";
  if(website == 0) {
    feast = 'flipkart';
  }
  else if(website == 1) {
    feast = 'snapdeal';
  }
  else if(website == 2) {
    feast = 'amazon';
  }
  var searchUrl = "http://50.112.38.102:7777/ping"
  var x = new XMLHttpRequest();
  x.open('POST', searchUrl);
  // The Google image search API responds with JSON, so let Chrome parse it.
  x.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  x.send(JSON.stringify({pid: pid, price: price, website: feast}));
  x.responseType = 'json';
  x.onload = function() {
    // Parse and process the response from Google Image Search.
    var response = x.response;
    if (!response || !response.advice || !response.confidence) {
      errorCallback('No response');
      return;
    }
    var advice = response.advice;
    var confidence = response.confidence;
    
    console.assert(
        !isNaN(parseInt(advice)) && !isNaN(parseInt(confidence)),
        'Unexpected respose from Search API!');
    callback(advice, confidence);
  };
  x.onerror = function() {
    errorCallback('Network error.');
  };
  //x.send();
}

function renderStatus(statusText) {
  document.getElementById('status').textContent = statusText;
}


document.addEventListener('DOMContentLoaded', function() {
  getCurrentTabUrl(function(url,website) {    
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
      chrome.tabs.sendMessage(tabs[0].id, {website: website}, function(response) {

        //if(!resp || !resp.price) return;
        var price = response.price;

        if(website == 0) {
          var pid = url.indexOf('?');
          url = url.substr(pid+1);
          var paramsArr = url.split("&");
          pid = "";
          for(var i = 0; i < paramsArr.length; i++) {
            if((paramsArr[i].split("="))[0] == "pid") {
              pid = (paramsArr[i].split("="))[1];
              break;
            }
          }
        }
        else if (website == 1) {
          var pid = url.indexOf('#');
          if(pid > -1) {
            url = url.substr(0, pid-1);
          }
          var paramsArr = url.split("/");
          pid = paramsArr[paramsArr.length - 1]
        }

        renderStatus('Performing Product search for ' + pid);


        getImageUrl(pid, price, website, function(advice, confidence) {

          renderStatus('Search term: ' + pid + '\n' +
            'Google image search result: ' + price);
          var imageResult = document.getElementById('advice');
          imageResult.innerHTML = "Advice: " + advice;
          imageResult.hidden = false;

          imageResult = document.getElementById('confidence');
          imageResult.innerHTML = "confidence: " + confidence;
          imageResult.hidden = false;

          imageResult = document.getElementById('status');
          imageResult.hidden = true;
        }, function(errorMessage) {
          renderStatus('Cannot display image. ' + errorMessage);
          } 
        );

      });
    });
    
  });
});
